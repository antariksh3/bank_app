package com.example.bankingapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Iterator;

import static com.example.bankingapp.R.color.colorPrimary;

public class TransferActivity extends AppCompatActivity implements View.OnClickListener {

    String username ;

    boolean isOwnAccountSelected ;
    ArrayList<User> masterList = new ArrayList() ;
    User currentUser ;
    TextView tvSavingsAcc , tvCheckingAcc ;
    RadioButton rbCheckingToSavings , rbSavingsToChecking , rbFromChecking , rbFromSavings ;
    RadioGroup rgOwnTransfer ;
    EditText etTransferAmount , etAccountNum ;
    Button btnOwnTransfer , btnOtherTransfer , btnTransferAmount , btnBackFromTransfer ;
    LinearLayout layout_other_transfer ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer);
        loadDataFromIntent();
        initializeView();
        isOwnAccountSelected = true ;
        initializeData();
        layout_other_transfer.setVisibility(View.GONE);

    }

    private void initializeData() {
        btnOtherTransfer.setBackgroundColor(Color.GRAY);
        tvSavingsAcc.setText("Savings Account" +
                "\nAccount Number : "+currentUser.getSavingsAccNum() +
                "\nAccount Balance : "+currentUser.getSavingsAmount());
        tvCheckingAcc.setText("Checking Amount"+
                "\nAccount Number : "+currentUser.getCheckingAccNum() +
                "\nAccount Balance : "+currentUser.getCheckingAmount());
    }

    private void initializeView() {
        tvSavingsAcc = findViewById(R.id.tv_savings_bal);
        tvCheckingAcc = findViewById(R.id.tv_checking_bal);
        rbCheckingToSavings = findViewById(R.id.rb_checking_to_savings);
        rbSavingsToChecking = findViewById(R.id.rb_savings_to_checking);
        etTransferAmount = findViewById(R.id.et_entered_amount);
        etAccountNum = findViewById(R.id.et_entered_account_num);
        btnOwnTransfer = findViewById(R.id.btn_own_account);
        btnOtherTransfer = findViewById(R.id.btn_other_account);
        btnTransferAmount = findViewById(R.id.btn_transfer_amount);
        rgOwnTransfer = findViewById(R.id.rg_own_account_type);
        layout_other_transfer = findViewById(R.id.layout_other_transfer);
        rbFromChecking = findViewById(R.id.rb_from_checking);
        rbFromSavings = findViewById(R.id.rb_from_savings);
        btnBackFromTransfer = findViewById(R.id.btn_back_from_transfer);

        btnBackFromTransfer.setOnClickListener(this);
        btnTransferAmount.setOnClickListener(this);
        btnOtherTransfer.setOnClickListener(this);
        btnOwnTransfer.setOnClickListener(this);

    }

    private void loadDataFromIntent() {
        Intent mIntent = getIntent();
        //masterList = (ArrayList[]) mIntent.getParcelableArrayExtra("USER_LIST");
        username = getIntent().getStringExtra("USER_NAME");
        MainActivity main = new MainActivity();
        masterList = (ArrayList<User>) MainActivity.getCustomSession().getUserList();
        //System.out.println(masterList.size());
        if (validateUsername(username)) {
            setCurrentUser(getUserBasedOnUsername(username));
        }
    }
    private User getUserBasedOnUsername(String username) {
        for (User user : masterList) {
            if(username.equalsIgnoreCase(user.getUsername())) {
                return user ;
            }
        }
        return null ;
    }

    private User getUserBasedOnBankAccount(int ba) {
        for (User user : masterList) {
            if(ba == user.getCheckingAccNum() || ba==user.getSavingsAccNum()) {
                return user ;
            }
        }
        return null ;
    }

    private boolean validateUsername(String username) {
        for (User user : masterList) {
            if(username.equalsIgnoreCase(user.getUsername())) {
                return true ;
            }
        }
        return false ;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_back_from_transfer) {
            finish();
        }
        if(v.getId() == R.id.btn_own_account) {
            rgOwnTransfer.setVisibility(View.VISIBLE);
            isOwnAccountSelected = true ;
            layout_other_transfer.setVisibility(View.GONE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btnOwnTransfer.setBackground(getDrawable(colorPrimary));
            }
            btnOtherTransfer.setBackgroundColor(Color.GRAY);
        }
        if(v.getId() == R.id.btn_other_account) {
            rgOwnTransfer.setVisibility(View.GONE);
            isOwnAccountSelected = false ;
            layout_other_transfer.setVisibility(View.VISIBLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btnOtherTransfer.setBackground(getDrawable(colorPrimary));
            }
            btnOwnTransfer.setBackgroundColor(Color.GRAY);
        }
        if(v.getId() == R.id.btn_transfer_amount){
            if(!btnTransferAmount.getText().toString().trim().equalsIgnoreCase("")){
                int amount = Integer.valueOf(etTransferAmount.getText().toString().trim());
                if (isOwnAccountSelected) {
                    // TRANSFER TO OWN ACCOUNT
                    if(rbCheckingToSavings.isChecked()){
                        // CHECKING TO SAVINGS
                        if (amount <= currentUser.getCheckingAmount()) {
                            double newSavingsAmount = amount + currentUser.getSavingsAmount();
                            currentUser.setSavingsAmount(newSavingsAmount);
                            double newCheckingAmount = currentUser.getCheckingAmount() - amount ;
                            currentUser.setCheckingAmount(newCheckingAmount);
                            updateCurentUserInMasterList(currentUser);
                            Toast.makeText(TransferActivity.this , "Transfer Success!",Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            // INSUFFICIENT BALANCE IN CHECKING ACCOUNT
                            displaySingleAlert("Insufficient Balance in Checking Account");
                        }
                    } else {
                        //SAVINGS TO CHECKING
                        if (amount <= currentUser.getSavingsAmount()) {
                            double newAmount = amount + currentUser.getCheckingAmount();
                            currentUser.setCheckingAmount(newAmount);
                            newAmount = currentUser.getSavingsAmount() - amount ;
                            currentUser.setSavingsAmount(newAmount);
                            updateCurentUserInMasterList(currentUser);
                            Toast.makeText(TransferActivity.this , "Transfer Success!",Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            // INSUFFICIENT BALANCE IN SAVINGS ACCOUNT
                            displaySingleAlert("Insufficient Balance in Savings Account");
                        }
                    }
                } else {
                    // TRANSFER TO OTHER ACCOUNT
                    String bankAccount = etAccountNum.getText().toString().trim();
                    if(!bankAccount.equalsIgnoreCase("")){
                        // CHECK IF ITS NOT THE SAME USERS BANK ACCOUNT
                        int bankAccountInt = Integer.parseInt(bankAccount);
                        if(bankAccountInt != currentUser.getCheckingAccNum()
                                && bankAccountInt != currentUser.getSavingsAccNum() ) {
                            char type = 'c';
                            if(rbFromChecking.isChecked()){
                                type = 'c' ;
                            } else {
                                type = 's' ;
                            }
                            if(validateBankAccount(bankAccountInt,username)){
                                boolean success = transferMoney(bankAccountInt,username,amount,type);
                                if(success) {
                                    Toast.makeText(TransferActivity.this,"Transfer Success!",Toast.LENGTH_SHORT).show();
                                    finish();
                                }
                            } else {
                                displaySingleAlert("Bank Account Number Not Found. Please Check Again");
                            }
                        } else {
                            displaySingleAlert("Please Do Not Enter Your Own Bank Account Numbers (Neither Checking, Nor Savings)");
                        }
                    } else {
                        displaySingleAlert("Please Enter a Bank Account Number");
                    }
                }
            } else {
                // AMOUNT NOT ENTERED
                displaySingleAlert("Please Enter Amount to be Transferred");
            }
        }
    }


    private boolean transferMoney(int reciverBankAccount, String senderUsername, int amount ,
                               char type ) {

        boolean noErrors = true ;

        //DEDUCT MONEY
        User myUser = getUserBasedOnUsername(senderUsername);
        if (type == 'c') {
            if(myUser.getCheckingAmount() < amount) {
                displaySingleAlert("Insufficient Balance for Transfer in your Checking Account");
                return false ;
            } else {
                myUser.setCheckingAmount(myUser.getCheckingAmount() - amount);
            }
        } else {
            if(myUser.getSavingsAmount() < amount) {
                displaySingleAlert("Insufficient Balance for Transfer in your Savings Account");
                return false ;
            } else {
                myUser.setSavingsAmount(myUser.getSavingsAmount() - amount);
            }
        }

        //SAVE
        if(noErrors){
            updateCurentUserInMasterList(myUser);
        }


        //ADD MONEY
        User thisUser = getUserBasedOnBankAccount(reciverBankAccount);
        if(thisUser.getCheckingAccNum()==reciverBankAccount){
            thisUser.setCheckingAmount( amount + thisUser.getCheckingAmount() );
        } else {
            thisUser.setSavingsAmount( amount + thisUser.getSavingsAmount() );
        }

        //SAVE
        if(noErrors) {
            updateCurentUserInMasterListBasedOnUsername(thisUser,thisUser.getUsername());
        }


        return noErrors ;

    }

    private boolean validateBankAccount(int bankAccountInt, String username) {
        for (User user : masterList) {
            if(!user.getUsername().equalsIgnoreCase(username)){
                if(user.getCheckingAccNum() == bankAccountInt
                        || user.getSavingsAccNum() == bankAccountInt ) {
                    return true ;
                }
            }
        }
        return false ;
    }

    private void updateCurentUserInMasterList(User currentUser) {
        Iterator<User> iter = masterList.iterator();
        while (iter.hasNext()){
            User u = iter.next();
            if (username.equalsIgnoreCase(u.getUsername())){
                iter.remove();
            }
        }
        masterList.add(currentUser);
        MainActivity.getCustomSession().setUserList(masterList);
    }

    private void updateCurentUserInMasterListBasedOnUsername(User currentUser, String usernameToRemove) {
        Iterator<User> iter = masterList.iterator();
        while (iter.hasNext()){
            User u = iter.next();
            if (usernameToRemove.equalsIgnoreCase(u.getUsername())){
                iter.remove();
            }
        }
        masterList.add(currentUser);
        MainActivity.getCustomSession().setUserList(masterList);
    }

    public void displaySingleAlert(final String heading) {
        new AlertDialog.Builder(TransferActivity.this)
                .setTitle("Information")
                .setMessage( heading )
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //
                    }
                })

                .setIcon(android.R.drawable.star_on)
                .show();
    }
}
