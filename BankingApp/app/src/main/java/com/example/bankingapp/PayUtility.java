package com.example.bankingapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;

public class PayUtility extends AppCompatActivity implements View.OnClickListener {

    String username ;
    CheckBox cbPhone , cbGas , cbWater , cbElectricity , cbSaveSubNum ;
    RadioGroup rgPayFrom ;
    RadioButton rbChecking , rbSaving ;
    TextView tvTotal ;
    Button btnPay , btnBackFromPayUtility ;
    EditText etSubNum ;
    int totalBillInt ;

    int billPhone = 0 , billGas = 0 , billWater = 0 , billElectricity = 0 , billTotal = 0 ;
    ArrayList<User> masterList = new ArrayList() ;
    User currentUser ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_utility);
        loadInitialView();
        loadDataFromIntent();
        loadViewData();
    }

    private void loadInitialView() {
        cbPhone = findViewById(R.id.cb_phone);
        cbGas = findViewById(R.id.cb_gas);
        cbWater = findViewById(R.id.cb_water);
        cbElectricity = findViewById(R.id.cb_electricity);
        cbSaveSubNum = findViewById(R.id.cb_save_sub_num);
        rgPayFrom = findViewById(R.id.rg_pay_from);
        rbChecking = findViewById(R.id.rb_checking);
        rbSaving = findViewById(R.id.rb_savings);
        tvTotal = findViewById(R.id.tv_total_bill);
        btnPay = findViewById(R.id.btn_pay);
        etSubNum = findViewById(R.id.et_sub_num);
        btnBackFromPayUtility = findViewById(R.id.btn_back_from_pay_utility);

        btnBackFromPayUtility.setOnClickListener(this);
        btnPay.setOnClickListener(this);
        cbElectricity.setOnClickListener(this);
        cbWater.setOnClickListener(this);
        cbGas.setOnClickListener(this);
        cbPhone.setOnClickListener(this);
        cbSaveSubNum.setOnClickListener(this);
    }


    private void loadViewData() {
        if(Utilities.get50PercentOdds() ){
            billPhone = Utilities.getRandomIntegerBetweenRange(10,100);
        } else {
            cbPhone.setEnabled(false);
        }
        if(Utilities.get50PercentOdds() ){
            billGas = Utilities.getRandomIntegerBetweenRange(10,100);
        } else {
            cbGas.setEnabled(false);
        }
        if(Utilities.get50PercentOdds() ){
            billWater = Utilities.getRandomIntegerBetweenRange(10,100);
        } else {
            cbWater.setEnabled(false);
        }
        if(Utilities.get50PercentOdds() ){
            billElectricity = Utilities.getRandomIntegerBetweenRange(10,100);
        } else {
            cbElectricity.setEnabled(false);
        }
        //billTotal = billElectricity + billWater + billGas + billPhone ;

        cbPhone.setHint("Phone Bill : $"+billPhone);
        cbGas.setHint("Gas Bill : $"+billGas);
        cbWater.setHint("Water Bill : $"+billWater);
        cbElectricity.setHint("Electricity Bill : $"+billElectricity);
        //tvTotal.setText("TOTAL BILL : $"+billTotal);

        rbSaving.setText("Savings Account (Balance $"+currentUser.getSavingsAmount()+" )");
        rbChecking.setText("Checking Account (Balance $"+currentUser.getCheckingAmount()+" )");

        if(currentUser.getSavedAccNum()!=0){
            etSubNum.setText(""+currentUser.getSavedAccNum());
        }

    }

    private void loadDataFromIntent() {
        Intent mIntent = getIntent();
        //masterList = (ArrayList[]) mIntent.getParcelableArrayExtra("USER_LIST");
        username = getIntent().getStringExtra("USER_NAME");
        MainActivity main = new MainActivity();
        masterList = (ArrayList<User>) MainActivity.getCustomSession().getUserList();
        //System.out.println(masterList.size());
        if (validateUsername(username)) {
            setCurrentUser(getUserBasedOnUsername(username));
        }
    }

    private User getUserBasedOnUsername(String username) {
        for (User user : masterList) {
            if(username.equalsIgnoreCase(user.getUsername())) {
                return user ;
            }
        }
        return null ;
    }

    private boolean validateUsername(String username) {
        for (User user : masterList) {
            if(username.equalsIgnoreCase(user.getUsername())) {
                return true ;
            }
        }
        return false ;
    }


    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_back_from_pay_utility) {
            finish();
        }
        if (v.getId() == R.id.cb_electricity || v.getId() == R.id.cb_gas ||
                v.getId() == R.id.cb_phone || v.getId() == R.id.cb_water) {
            totalBillInt = calculateTotalBill();
            tvTotal.setText("TOTAL BILL : $"+totalBillInt);
        }
        if (v.getId() == R.id.btn_pay) {

            if(cbSaveSubNum.isChecked() && !etSubNum.getText().toString().trim().equalsIgnoreCase("")){
                currentUser.setSavedAccNum(Long.valueOf(etSubNum.getText().toString().trim()));
            } else {
                currentUser.setSavedAccNum(0L);
            }

            if(rbChecking.isChecked()) {
                if ( totalBillInt > 0 && currentUser.getCheckingAmount() >= totalBillInt) {
                    new AlertDialog.Builder(PayUtility.this)
                            .setTitle("Pay Bill")
                            .setMessage("Are you sure you want to Pay $"+totalBillInt
                            +"\nUpdated Balance in Checking Account will be : $"+
                                    (currentUser.getCheckingAmount() - Double.valueOf(totalBillInt)))
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    currentUser.setCheckingAmount((currentUser.getCheckingAmount()
                                            - Double.valueOf(totalBillInt)));
                                    //INCREASE CREDIT SCORE
                                    currentUser.setCreditScore(currentUser.getCreditScore()
                                            + Utilities.getRandomIntegerBetweenRange(80,150) );
                                    updateCurentUserInMasterList(currentUser);
                                    finish();
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            })
                            .setIcon(android.R.drawable.star_on)
                            .show();
                } else {
                    if(currentUser.getCheckingAmount() < totalBillInt) {
                        displaySingleAlert("Insufficient Balance.");
                    } else if (totalBillInt==0){
                        displaySingleAlert("Please Select One or More Bills to Pay.");
                    }

                }
            }
            if(rbSaving.isChecked()) {
                if ( totalBillInt > 0 && currentUser.getSavingsAmount() >= totalBillInt) {
                    new AlertDialog.Builder(PayUtility.this)
                            .setTitle("Pay Bill")
                            .setMessage("Are you sure you want to Pay $"+totalBillInt
                                    +"\nUpdated Balance in Savings Account will be : $"+
                                    (currentUser.getSavingsAmount() - Double.valueOf(totalBillInt)))
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    currentUser.setSavingsAmount((currentUser.getSavingsAmount()
                                            - Double.valueOf(totalBillInt)));
                                    //INCREASE CREDIT SCORE
                                    currentUser.setCreditScore(currentUser.getCreditScore()
                                            + Utilities.getRandomIntegerBetweenRange(80,150) );
                                    updateCurentUserInMasterList(currentUser);
                                    finish();
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            })
                            .setIcon(android.R.drawable.star_on)
                            .show();
                } else {
                    if(currentUser.getSavingsAmount() < billTotal) {
                        displaySingleAlert("Insufficient Balance.");
                    }
                    if(billTotal==0){
                        displaySingleAlert("Please Select One or More Bills to Pay.");
                    }

                }
            }
        }
    }

    private void updateCurentUserInMasterList(User currentUser) {
        Iterator<User> iter = masterList.iterator();
        while (iter.hasNext()){
            User u = iter.next();
            if (username.equalsIgnoreCase(u.getUsername())){
                iter.remove();
            }
        }
        masterList.add(currentUser);
        MainActivity.getCustomSession().setUserList(masterList);
    }

    private int calculateTotalBill() {
        int localTotal = 0 ;
        if(cbPhone.isChecked()){
            localTotal += billPhone ;
        }
        if(cbGas.isChecked()){
            localTotal += billGas ;
        }
        if(cbWater.isChecked()){
            localTotal += billWater ;
        }
        if(cbElectricity.isChecked()){
            localTotal += billElectricity ;
        }
        return localTotal ;
    }

    public void displaySingleAlert(final String heading) {
        new AlertDialog.Builder(PayUtility.this)
                .setTitle("Information")
                .setMessage( heading )
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //
                    }
                })

                .setIcon(android.R.drawable.star_on)
                .show();
    }
}
