package com.example.bankingapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class CheckCreditScore extends AppCompatActivity implements View.OnClickListener {

    int score;
    TextView tvScoreDescription , tvScoreNumber ;
    ImageView ivScore ;
    Button btnBackFromCreditScore ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_credit_score);
        initializeView();
        loadDataFromIntent();

    }

    private void initializeView() {
        tvScoreDescription = findViewById(R.id.tv_score_description);
        tvScoreNumber = findViewById(R.id.tv_score_number);
        ivScore = findViewById(R.id.iv_score);
        btnBackFromCreditScore = findViewById(R.id.btn_back_from_credit_score);
        btnBackFromCreditScore.setOnClickListener(this);
    }

    private void loadDataFromIntent() {
        Intent mIntent = getIntent();
        score = getIntent().getIntExtra("CREDIT_SCORE",450);
        if(score < 450) {
            tvScoreNumber.setText(""+score);
            tvScoreDescription.setText("POOR");
            ivScore.setImageResource(R.drawable.score_red);
        } else if (score <= 600 ) {
            tvScoreNumber.setText(""+score);
            tvScoreDescription.setText("FAIR");
            ivScore.setImageResource(R.drawable.score_orange);
        } else if (score <= 750) {
            tvScoreNumber.setText(""+score);
            tvScoreDescription.setText("GOOD");
            ivScore.setImageResource(R.drawable.score_yellow);
        } else if (score < 900) {
            tvScoreNumber.setText(""+score);
            tvScoreDescription.setText("VERY GOOD");
            ivScore.setImageResource(R.drawable.score_green);
        } else if (score >= 900) {
            tvScoreNumber.setText("900");
            tvScoreDescription.setText("PREFECT");
            ivScore.setImageResource(R.drawable.score_dark_green);
        }
    }


    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_back_from_credit_score) {
            finish();
        }
    }
}
