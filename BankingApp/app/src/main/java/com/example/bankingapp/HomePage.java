package com.example.bankingapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class HomePage extends AppCompatActivity implements View.OnClickListener {

    TextView tvWelcome , tvCheckingAccNum , tvCheckingBalance ;
    TextView tvSavingsAccNum , tvSavingsBalance ;
    TextView tvPayBills , tvCheckCreditScore ;
    Button btnTransfer , btnLogout ;
    ArrayList<User> masterList = new ArrayList() ;
    String username ;
    User currentUser ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        loadDataFromIntent();
        initializeDisplay();

    }

    private void loadDataFromIntent() {
        Intent mIntent = getIntent();
        //masterList = (ArrayList[]) mIntent.getParcelableArrayExtra("USER_LIST");
        username = getIntent().getStringExtra("USER_NAME");
        MainActivity main = new MainActivity();
        masterList = (ArrayList<User>) MainActivity.getCustomSession().getUserList();
        //System.out.println(masterList.size());
        if (validateUsername(username)) {
            setCurrentUser(getUserBasedOnUsername(username));
        }
    }

    private void initializeDisplay() {
        tvWelcome = findViewById(R.id.tv_welcome);
        tvCheckingAccNum = findViewById(R.id.tv_checking_acc_num);
        tvCheckingBalance = findViewById(R.id.checking_acc_bal);
        tvSavingsAccNum = findViewById(R.id.tv_savings_acc_num);
        tvSavingsBalance = findViewById(R.id.tv_savings_acc_bal);
        tvPayBills = findViewById(R.id.tv_pay_bills);
        tvPayBills.setOnClickListener(this);
        tvCheckCreditScore = findViewById(R.id.tv_check_credit_score);
        tvCheckCreditScore.setOnClickListener(this);
        btnTransfer = findViewById(R.id.btn_transfer);
        btnTransfer.setOnClickListener(this);
        btnLogout = findViewById(R.id.btn_logout);
        btnLogout.setOnClickListener(this);
        loadDisplay();
    }

    private void loadDisplay() {
        User currentUser = getUserBasedOnUsername(username);
        if(currentUser != null) {
            tvWelcome.setText("Welcome, "+currentUser.getFirstName()+" "+currentUser.getLastName());
            tvCheckingAccNum.setText("Checking Account Number : "+currentUser.getCheckingAccNum());
            tvCheckingBalance.setText("Balance : "+currentUser.getCheckingAmount());
            tvSavingsAccNum.setText("Savings Account Number : "+currentUser.getSavingsAccNum());
            tvSavingsBalance.setText("Balance : "+currentUser.getSavingsAmount());
        }
    }

    private User getUserBasedOnUsername(String username) {
        for (User user : masterList) {
            if(username.equalsIgnoreCase(user.getUsername())) {
                return user ;
            }
        }
        return null ;
    }

    private boolean validateUsername(String username) {
        for (User user : masterList) {
            if(username.equalsIgnoreCase(user.getUsername())) {
                return true ;
            }
        }
        return false ;
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_logout) {
            finish();
        }
        if(v.getId() == R.id.tv_pay_bills) {
            Intent intent = new Intent(HomePage.this, PayUtility.class);
            //bundle.putSerializable("MSATER_LIST", (Serializable) masterList);
            intent.putExtra("USER_NAME", username);
            //intent.putExtra(bundle);

            startActivity(intent);
        }
        if(v.getId()==R.id.btn_transfer){
            Intent intent = new Intent(HomePage.this, TransferActivity.class);
            //bundle.putSerializable("MSATER_LIST", (Serializable) masterList);
            intent.putExtra("USER_NAME", username);
            //intent.putExtra(bundle);

            startActivity(intent);
        }
        if(v.getId()==R.id.tv_check_credit_score) {
            Intent intent = new Intent(HomePage.this, CheckCreditScore.class);
            //bundle.putSerializable("MSATER_LIST", (Serializable) masterList);
            intent.putExtra("CREDIT_SCORE", currentUser.getCreditScore());
            //intent.putExtra(bundle);

            startActivity(intent);
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        loadDisplay();
    }

    public void displaySingleAlert(final String heading) {
        new AlertDialog.Builder(HomePage.this)
                .setTitle("Pay Bill")
                .setMessage("Are you sure you want to Pay $40 for" + heading )
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        displayMessageAlert(heading+" Paid");
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(android.R.drawable.star_on)
                .show();
    }

    public void displayMessageAlert(String msg) {
        System.out.println("LOGIN FAILED");
        new AlertDialog.Builder(HomePage.this)
                .setTitle("Information")
                .setMessage(msg)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Continue with delete operation
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }
    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }


}
