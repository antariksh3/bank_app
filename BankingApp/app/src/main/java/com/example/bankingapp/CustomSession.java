package com.example.bankingapp;

import java.util.ArrayList;
import java.util.List;

public class CustomSession {

    private List<User> userList ;

    public List<User> getUserList() {
        if ( userList == null ) {
            return new ArrayList<User>();
        }
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }
}
