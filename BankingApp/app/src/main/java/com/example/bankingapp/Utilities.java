package com.example.bankingapp;


public class Utilities {

    private static final int ACCOUNT_NUM_MIN = 10000 ;
    private static final int ACCOUNT_NUM_MAX = 99999 ;
    private static final int UTILITY_BILL_MIN = 5 ;
    private static final int UTILITY_BILL_MAX = 150 ;

    public static int getRandomIntegerBetweenRange(int min, int max){
        int randomNum = (int) ((Math.random()*((max-min)+1))+min);
        return randomNum;
    }

    public static boolean get50PercentOdds() {
        return (getRandomIntegerBetweenRange(1,2)%2==0) ? true : false ;
    }

    public static int generateUtilityBill() {
        return getRandomIntegerBetweenRange(UTILITY_BILL_MIN, UTILITY_BILL_MAX);
    }
}