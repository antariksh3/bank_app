package com.example.bankingapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText etUsername , etPassword ;
    Button btnLogin ;
    List<User> masterList ;

    public List<User> getMasterList() {
        return masterList;
    }
    public void setMasterList(List<User> masterList) {
        this.masterList = masterList;
    }

    public static CustomSession customSession = new CustomSession();

    public static CustomSession getCustomSession() {
        return customSession;
    }
    public static void setCustomSession(CustomSession customSession) {
        MainActivity.customSession = customSession;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeUserList();
        initializeView();
    }

    private void initializeView() {
        etUsername = findViewById(R.id.et_username);
        etPassword = findViewById(R.id.et_password);
        btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(this);

    }



    private void initializeUserList() {
        User u1 = new User("1001", "1234" , "Antariksh" , "Gosain",
        12345 , 334455 , 43434355 , 200.0 ,
                300.0, 670 , 500 , 0 );
        User u2 = new User("2002", "4567" , "Abhishek" , "Kapoor",
                56789 , 994455 , 43565656 , 300.0 ,
                200.0, 440 , 300 , 0 );
        User u3 = new User("3003", "7890" , "Emad" , "Sir",
                45678 , 334466 , 54434343 , 500.0 ,
                2000.0,300 , 800 , 0 );

        List<User> tempList = new ArrayList<User>();
        tempList.add(u1);
        tempList.add(u2);
        tempList.add(u3);

        masterList = tempList ;
        customSession.setUserList(masterList);

    }

    @Override
    public void onClick(View v) {
        //Login Button Clicked
        List<User> tempList = masterList;

        if(v.getId() == R.id.btn_login) {
            if ( tempList!=null && etUsername.getText().toString().trim()!="" && etPassword.getText().toString().trim()!="" ) {
                if (checkUserNameAndPassword(etUsername.getText().toString().trim() ,
                        etPassword.getText().toString().trim() , tempList)) {
                    //LOGIN SUCCESS
                    System.out.println("LOGGED IN : "+etUsername.getText().toString().trim());
                    Intent intent = new Intent(MainActivity.this, HomePage.class);
                    //bundle.putSerializable("MSATER_LIST", (Serializable) masterList);
                    intent.putExtra("USER_NAME", etUsername.getText().toString().trim());
                    //intent.putExtra(bundle);

                    startActivity(intent);
                } else {
                    //LOGIN FAILED
                    System.out.println("LOGIN FAILED");
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Invalid Credentials")
                            .setMessage("Username and/or Password Did Not Match " +
                                    "\nPlease Try Again")
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Continue with delete operation
                                }
                            })
                            .setIcon(android.R.drawable.stat_notify_error)
                            .show();
                }

            }
        }
    }

    private boolean checkUserNameAndPassword(String username, String password, List<User> tempList) {

        for (User user : tempList) {
            if(username.equalsIgnoreCase(user.getUsername()) && password.equalsIgnoreCase(user.getPassword())) {
                return true ;
            }
        }
        return false ;
    }
}
