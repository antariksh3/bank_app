package com.example.bankingapp;

public class User {

    private String username ;
    private String password ;
    private String firstName ;
    private String lastName ;
    private int checkingAccNum ;
    private int savingsAccNum ;
    private int creditCardNum ;
    private double checkingAmount ;
    private double savingsAmount ;
    private double creditCardAmount ;
    private int creditScore ;
    private long savedAccNum ;



    public User(String username, String password, String firstName, String lastName,
                int checkingAccNum, int savingsAccNum, int creditCardNum, Double checkingAmount,
                Double savingsAmount, double creditCardAmount, int creditScore , long savedAccNum) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.checkingAccNum = checkingAccNum;
        this.savingsAccNum = savingsAccNum;
        this.creditCardNum = creditCardNum;
        this.checkingAmount = checkingAmount;
        this.savingsAmount = savingsAmount;
        this.creditCardAmount = creditCardAmount;
        this.creditScore = creditScore;
        this.savedAccNum = savedAccNum;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", checkingAccNum=" + checkingAccNum +
                ", savingsAccNum=" + savingsAccNum +
                ", creditCardNum=" + creditCardNum +
                ", checkingAmount=" + checkingAmount +
                ", savingsAmount=" + savingsAmount +
                ", creditCardAmount=" + creditCardAmount +
                ", creditScore=" + creditScore +
                ", savedAccNum=" + savedAccNum +
                '}';
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getCheckingAccNum() {
        return checkingAccNum;
    }

    public void setCheckingAccNum(int checkingAccNum) {
        this.checkingAccNum = checkingAccNum;
    }

    public int getSavingsAccNum() {
        return savingsAccNum;
    }

    public void setSavingsAccNum(int savingsAccNum) {
        this.savingsAccNum = savingsAccNum;
    }

    public int getCreditCardNum() {
        return creditCardNum;
    }

    public void setCreditCardNum(int creditCardNum) {
        this.creditCardNum = creditCardNum;
    }

    public double getCheckingAmount() {
        return checkingAmount;
    }

    public void setCheckingAmount(double checkingAmount) {
        this.checkingAmount = checkingAmount;
    }

    public double getSavingsAmount() {
        return savingsAmount;
    }

    public void setSavingsAmount(double savingsAmount) {
        this.savingsAmount = savingsAmount;
    }

    public double getCreditCardAmount() {
        return creditCardAmount;
    }

    public void setCreditCardAmount(double creditCardAmount) {
        this.creditCardAmount = creditCardAmount;
    }

    public int getCreditScore() {
        return creditScore;
    }

    public void setCreditScore(int creditScore) {
        this.creditScore = creditScore;
    }

    public long getSavedAccNum() {
        return savedAccNum;
    }

    public void setSavedAccNum(long savedAccNum) {
        this.savedAccNum = savedAccNum;
    }
}
